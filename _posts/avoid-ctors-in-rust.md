---
layout: post
title: How to avoid writing constructors in Rust and benefit `From` it
---

- Rust has traits, which are awesome
- some traits let us write constructor equivalents
- these equivalents are more powerful because of the trait being implemented

## `Default` Constructors

```rust
pub struct Second {
    value: u64
}

impl Second {
    pub fn new() -> Self {
        Self { value: 0 }
    }
}

fn main() {
    let m = Second::new();
}
```

```rust
pub struct Second {
    value: u64
}

impl Default for Second {
    fn default() -> Self {
        Self { value: 0 }
    }
}

fn main() {
    let m = Second::default();
}
```

```rust
#[derive(Default)]
pub struct Second {
    value: u64
}

fn main() {
    let m = Second::default();
}
```

- now, the type can also be used where `Default` is required
- most prominently, any of the [`*or_default`][std-or_default] functions on the
  types in the standard library
- also show other example of type with multiple members using
  `..Default::default()` constructor

## `From`, `TryFrom`, `FromStr` Constructors

```rust
pub struct Second {
    value: u64
}

impl Second {
    pub fn new(value: u64) -> Self {
        Self { value }
    }
}
```

```rust
pub struct Second {
    value: u64
}

impl From<u64> for Second {
    fn from(value: u64) -> Self {
        Self { value }
    }
}
```

## Builder Pattern

## Drawbacks

- less discoverable, especially for newcomers to Rust who are not familiar with
  these traits
- API shows traits not prominently
- can be handled by providing appropriate API documentation for the type


[std-default]: https://doc.rust-lang.org/stable/std/default/trait.Default.html
[std-from]: https://doc.rust-lang.org/stable/std/convert/trait.From.html
[std-tryfrom]: https://doc.rust-lang.org/stable/std/convert/trait.TryFrom.html
[std-fromstr]: https://doc.rust-lang.org/stable/std/str/trait.FromStr.html
[std-or_default]: https://doc.rust-lang.org/stable/std/?search=or_default
